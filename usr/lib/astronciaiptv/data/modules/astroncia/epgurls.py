'''
Copyright (C) 2021 Astroncia

    This file is part of Astroncia IPTV.

    Astroncia IPTV is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Astroncia IPTV is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Astroncia IPTV.  If not, see <https://www.gnu.org/licenses/>.
'''
EPG_URLS = [
    "http://www.teleguide.info/download/new3/xmltv.xml.gz",
    "https://iptvx.one/EPG_LITE",
    "http://programtv.ru/xmltv.xml.gz",
    "http://epg.it999.ru/edem.xml.gz",
    "https://iptv-org.github.io/epg/guides/tv.yandex.ru.guide.xml"
]
