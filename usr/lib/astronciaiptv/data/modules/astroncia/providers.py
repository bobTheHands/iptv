'''IPTV providers URLs'''
'''
Copyright (C) 2021 Astroncia

    This file is part of Astroncia IPTV.

    Astroncia IPTV is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Astroncia IPTV is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Astroncia IPTV.  If not, see <https://www.gnu.org/licenses/>.
'''
iptv_providers = {
    "[Worldwide] Free-TV": {
        'm3u': 'https://raw.githubusercontent.com/Free-TV/IPTV/master/playlist.m3u8'
    },
    "[Интернет-провайдер] SkyNet (Россия, Санкт-Петербург)": {
        'm3u': 'http://m3u.sknt.ru/cat/',
        'epg': 'http://astroncia.crabdance.com:59642/epg.xml.gz'
    }
}
